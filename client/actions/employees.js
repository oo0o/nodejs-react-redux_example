import * as types from '../constants/employees';

import employeesApi from '../api/employee';

export function fetchEmployees() {
  return function(dispatch) {
    return employeesApi.fetchEmployees().then(employees => {
      dispatch(fetchEmployeesSuccess(employees));
    }).catch(error => {
      throw(error);
    });
  };
}

export function fetchEmployeesSuccess(employees) {
  return {
    type: types.FETCH_EMPLOYEES_SUCCESS,
    employees
  };
}

export function fetchEmployee(id) {
  return function(dispatch) {
    return employeesApi.getEmployee(id).then(employee => {
      dispatch(fetchEmployeeSuccess(employee));
    }).catch(error => {
      throw(error);
    });
  };
}

export function fetchEmployeeSuccess(employee) {
  return {
    type: types.FETCH_EMPLOYEE_SUCCESS,
    employee
  };
}

export function saveEmployee(employee) {
  return function(dispatch) {
    return employeesApi.saveEmployee(employee).then(employee => {
      dispatch(saveEmployeeSuccess(employee));
    }).catch(error => {
      throw(error);
    });
  };
}

export function saveEmployeeSuccess(employee) {
  return {
    type: types.SAVE_EMPLOYEE_SUCCESS,
    employee
  };
}

export function createEmployee(employee) {
  return function(dispatch) {
    return employeesApi.createEmployee(employee).then(employee => {
      dispatch(createEmployeeSuccess(employee));
    }).catch(error => {
      throw(error);
    });
  };
}

export function createEmployeeSuccess(employee) {
  return {
    type: types.CREATE_EMPLOYEE_SUCCESS,
    employee
  }
}

export function destroyEmployee(id) {
  return function(dispatch) {
    return employeesApi.destroyEmployee(id).then(employee => {
      dispatch(destroyEmployeeSuccess(employee));
    }).catch(error => {
      throw(error);
    });
  };
}

export function destroyEmployeeSuccess(employee) {
  return {
    type: types.DESTROY_EMPLOYEE_SUCCESS,
    employeeId: employee.id
  };
}

