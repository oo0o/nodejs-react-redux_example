import * as types from '../constants/departments';

import departmentsApi from '../api/departments';

export function fetchDepartments() {
  return function(dispatch) {
    return departmentsApi.fetchDepartments().then(departments => {
      dispatch(fetchDepartmentsSuccess(departments));
    }).catch(error => {
      throw(error);
    });
  };
}

export function fetchDepartmentsSuccess(departments) {
  return {
    type: types.FETCH_DEPARTMENTS_SUCCESS,
    departments
  };
}

export function fetchDepartment(id) {
  return function(dispatch) {
    return departmentsApi.getDepartment(id).then(department => {
      dispatch(fetchDepartmentSuccess(department));
    }).catch(error => {
      throw(error);
    });
  };
}

export function fetchDepartmentSuccess(department) {
  return {
    type: types.FETCH_DEPARTMENT_SUCCESS,
    department
  };
}

export function saveDepartment(department) {
  return function(dispatch) {
    return departmentsApi.saveDepartment(department).then(department => {
      dispatch(saveDepartmentSuccess(department));
    }).catch(error => {
      throw(error);
    });
  };
}

export function saveDepartmentSuccess(department) {
  return {
    type: types.SAVE_DEPARTMENT_SUCCESS,
    department
  }
}

export function createDepartment(department) {
  return function(dispatch) {
    return departmentsApi.createDepartment(department).then(department => {
      dispatch(createDepartmentSuccess(department));
    }).catch(error => {
      throw(error);
    });
  };
}

export function createDepartmentSuccess(department) {
  return {
    type: types.CREATE_DEPARTMENT_SUCCESS,
    department
  };
}

export function destroyDepartment(id) {
  return function(dispatch) {
    return departmentsApi.destroyDepartment(id).then((department) => {
      dispatch(destroyDepartmentSuccess(department));
    }).catch(error => {
      throw(error);
    });
  };
}

export function destroyDepartmentSuccess(department) {
  return {
    type: types.DESTROY_DEPARTMENT_SUCCESS,
    departmentId: department.id
  };
}

