import React from 'react';
import * as h from '../../lib/helpers';

//import bootstrap
import Panel from 'react-bootstrap/lib/Panel';
import Form from 'react-bootstrap/lib/Form';
import FormGroup from 'react-bootstrap/lib/FormGroup';
import Col from 'react-bootstrap/lib/Col';
import FormControl from 'react-bootstrap/lib/FormControl';
import ControlLabel from 'react-bootstrap/lib/ControlLabel';

const CurrentEmployeeInfo = (props) => {
  return (
    <div>
      <Panel header="Current info" bsStyle="info">
        <Form horizontal>
          <FormGroup>
            <Col componentClass={ControlLabel} sm={2}>
              <div className="info-title">
                Id
              </div>
            </Col>
            <Col sm={10}>
              <FormControl
                value={props.singleEmployee.id || ''}
                disabled
              />
            </Col>
          </FormGroup>
          <FormGroup>
            <Col componentClass={ControlLabel} sm={2}>
              <div className="info-title">
                First Name
              </div>
            </Col>
            <Col sm={10}>
              <FormControl
                value={props.singleEmployee.firstName || ''}
                disabled
              />
            </Col>
          </FormGroup>
          <FormGroup>
            <Col componentClass={ControlLabel} sm={2}>
              <div className="info-title">
                Last Name
              </div>
            </Col>
            <Col sm={10}>
              <FormControl
                value={props.singleEmployee.lastName || ''}
                disabled
              />
            </Col>
          </FormGroup>
          <FormGroup>
            <Col componentClass={ControlLabel} sm={2}>
              <div className="info-title">
                Department Id
              </div>
            </Col>
            <Col sm={10}>
              <FormControl
                value={props.singleEmployee.departmentId || ''}
                disabled
              />
            </Col>
          </FormGroup>
        </Form>
      </Panel>
    </div>
  );
};

export default CurrentEmployeeInfo;
