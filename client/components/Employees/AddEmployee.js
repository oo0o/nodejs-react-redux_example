import React from 'react';

// import bootstrap
import Panel from 'react-bootstrap/lib/Panel';
import Accordion from 'react-bootstrap/lib/Accordion';
import Form from 'react-bootstrap/lib/Form';
import FormGroup from 'react-bootstrap/lib/FormGroup';
import Col from 'react-bootstrap/lib/Col';
import FormControl from 'react-bootstrap/lib/FormControl';
import ControlLabel from 'react-bootstrap/lib/ControlLabel';

const AddEmployee = (props) => {
  return (
    <div className="grid-item">
      <Accordion>
        <Panel header="Add employee" eventKey="1">
          <Form horizontal onSubmit={props.handleCreate}>
            <FormGroup>
              <Col componentClass={ControlLabel} sm={2}>
                <div className="info-title">
                  First Name
                </div>
              </Col>
              <Col sm={10}>
                <FormControl
                  type="text"
                  name="firstName"
                  value={props.currentEmployee.firstName || ''}
                  placeholder="Enter employee first name"
                  onChange={props.handleInputChange}
                />
              </Col>
            </FormGroup>
            <FormGroup>
              <Col componentClass={ControlLabel} sm={2}>
                <div className="info-title">
                  Last Name
                </div>
              </Col>
              <Col sm={10}>
                <FormControl
                  type="text"
                  name="lastName"
                  value={props.currentEmployee.lastName || ''}
                  placeholder="Enter employee last name"
                  onChange={props.handleInputChange}
                />
              </Col>
            </FormGroup>
            <FormGroup controlId="formControlsSelect">
             <Col componentClass={ControlLabel} sm={2}>
                <div className="info-title">
                  Department
                </div>
              </Col>
              <Col sm={10}>
              <FormControl 
                componentClass="select" 
                name="departmentId"
                placeholder="select"
                onChange={props.handleInputChange}>
                <option value="select">Select Department for Employee</option>
                {props.departments.map(department => (<option value={department.id} key={department.id}>{department.name}</option>))}
              </FormControl>
              </Col>
            </FormGroup>
            <div className="control-buttons">
              <button className="add" type="submit">Add</button>
            </div>
          </Form>
        </Panel>
      </Accordion>
    </div>
  );
};

export default AddEmployee;
