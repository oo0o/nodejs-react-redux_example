import { bindActionCreators }       from 'redux';
import { connect }                  from 'react-redux';
import * as actionCreatorsEmployees from '../../actions/employees';
import * as actionCreatorsDepartments from '../../actions/departments';
import EmployeesGrid                from './EmployeesGrid';

function mapStateToProps(state) {
  return {
    departments: state.departments,
    employees: state.employees
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({...actionCreatorsEmployees, ...actionCreatorsDepartments}, dispatch);
}

const _EmployeesGrid = connect(mapStateToProps, mapDispatchToProps)(EmployeesGrid);

export default _EmployeesGrid;
