import React, { Component } from 'react';
import { Link } from 'react-router';
import { partial } from '../../lib/utils';

class Employee extends Component {

  render() {
    const { employee, i } = this.props;
    const destroyEmployee = partial(this.props.destroyEmployee, employee.id);
    return (
        <div className='grid-item'>
          <div className="item-caption">
            <Link to={`/employee/${employee.id}`} className='link'>
              {employee.firstName} {employee.lastName}
            </Link>
          </div>
          <div className="control-buttons">
            <Link to={`/employee/${employee.id}`} className="button">
              Edit
            </Link>
            <button className="delete" onClick={destroyEmployee}>Delete</button>
          </div>    
        </div>
    );
  }
}

export default Employee;
