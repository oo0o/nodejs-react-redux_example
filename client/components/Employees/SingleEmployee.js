import React, { Component } from 'react';
import Employee from './Employee';
import * as h from '../../lib/helpers';

import EditEmployee from './EditEmployee';
import CurrentEmployeeInfo from './CurrentEmployeeInfo';

class SingleEmployee extends Component {

  state = {
    currentEmployee: {
      firstName: '',
      lastName: '',
      departmentId: ''
    }
  }
  
  componentWillMount() {
    this.props.fetchEmployees()
      .then(this.props.fetchEmployee(+this.props.params.employeeId))
      .then(this.props.fetchDepartments());
  }

  componentWillReceiveProps(nextProps) {
    const employee = nextProps.singleEmployee;
    this.setState({
      currentEmployee: {
        firstName: employee.firstName,
        lastName: employee.lastName,
        departmentId: employee.departmentId
      }
    });
  }

  handleInputChange = (evt) => {
    const target = evt.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      currentEmployee: {...this.state.currentEmployee, [name]: value},

    });
  }

  handleUpdate = (evt) => {
    evt.preventDefault();
    const previousEmployee = h.findById(this.props.singleEmployee.id, this.props.employees);
    const updatedEmployee = h.updateItem(previousEmployee, this.state.currentEmployee);
    this.props.saveEmployee(updatedEmployee);
  }
  
  render() {
    return (
      <div className="single-employee">
        <h2>Edit employee</h2>
        <div className="info">
          <CurrentEmployeeInfo 
            singleEmployee={this.props.singleEmployee}
            departments={this.props.departments}
          />
          <EditEmployee 
            handleUpdate={this.handleUpdate}
            currentEmployee={this.state.currentEmployee}
            handleInputChange={this.handleInputChange}
            departments={this.props.departments}
          />
        </div>
      </div>
    );
  }
}

export default SingleEmployee;
