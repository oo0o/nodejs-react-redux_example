import React, { Component } from 'react';

// import components
import Employee from './Employee';
import AddEmployee from './AddEmployee';

class EmployeesGrid extends Component {

  state = {
    currentEmployee: {
      firstName: '',
      lastName: '',
      departmentId: ''
    }
  }

  componentWillMount() {
    this.props.fetchEmployees();
    this.props.fetchDepartments();
  }

  handleInputChange = (evt) => {
    const target = evt.target;
    const  value = target.value;
    const   name = target.name;
    this.setState({
      currentEmployee: {...this.state.currentEmployee, [name]: value}
    });
  }

  handleCreate = (evt) => {
    evt.preventDefault();
    const newEmployee = {...this.state.currentEmployee};
    this.props.createEmployee(newEmployee).then(
      this.setState({
        currentEmployee: {
          firstName: '',
          lastName: '',
          departmentId: ''
        }
      })
    )
  }
  
  render() {
    return (
      <div className="grid">
        <AddEmployee 
          handleCreate={this.handleCreate}
          currentEmployee={this.state.currentEmployee}
          handleInputChange={this.handleInputChange}
          departments={this.props.departments}
          selectedDepartment={this.state.selectedDepartment}
        />
        {this.props.employees.map((employee, i) => <Employee {...this.props} key={employee.id} employee={employee} />)}
      </div>
    );
  }
}

export default EmployeesGrid;
