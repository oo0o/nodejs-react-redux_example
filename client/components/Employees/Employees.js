import React, { Component } from 'react';

import { Link } from 'react-router';

class Employees extends Component {
  render() {
    return (
      <div>
        {this.props.children}
      </div>
    );
  }
}

export default Employees;
