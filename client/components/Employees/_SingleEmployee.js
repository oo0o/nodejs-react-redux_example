import { bindActionCreators }         from 'redux';
import { connect }                    from 'react-redux';
import * as actionCreatorsDepartments from '../../actions/departments';
import * as actionCreatorsEmployees   from '../../actions/employees';
import SingleEmployee                 from './SingleEmployee';

function mapStateToProps(state) {
  return {
           employees: state.employees,
      singleEmployee: state.singleEmployee,
         departments: state.departments,
    singleDepartment: state.singleDepartment
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({...actionCreatorsDepartments, ...actionCreatorsEmployees}, dispatch);
}

const _SingleEmployee = connect(mapStateToProps, mapDispatchToProps)(SingleEmployee);

export default _SingleEmployee;

