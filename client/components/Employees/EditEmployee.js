import React from 'react';

//import Bootstrap
import Panel from 'react-bootstrap/lib/Panel';
import Form from 'react-bootstrap/lib/Form';
import FormGroup from 'react-bootstrap/lib/FormGroup';
import Col from 'react-bootstrap/lib/Col';
import FormControl from 'react-bootstrap/lib/FormControl';
import ControlLabel from 'react-bootstrap/lib/ControlLabel';
import Button from 'react-bootstrap/lib/Button';

const EditEmployee = (props) => {
  return (
    <Panel header="New info" bsStyle="success">
      <Form horizontal onSubmit={props.handleUpdate}>
        <FormGroup>
          <Col componentClass={ControlLabel} sm={2}>
            <div className="info-title">
              First Name
            </div>
          </Col>
          <Col sm={10}>
            <FormControl
              type="text"
              name="firstName"
              value={props.currentEmployee.firstName || ''}
              placeholder="Enter employee first name"
              onChange={props.handleInputChange}
            />
          </Col>
        </FormGroup>
        <FormGroup>
          <Col componentClass={ControlLabel} sm={2}>
            <div className="info-title">
              Last Name
            </div>
          </Col>
          <Col sm={10}>
            <FormControl
              type="text"
              name="lastName"
              value={props.currentEmployee.lastName || ''}
              placeholder="Enter employee last name"
              onChange={props.handleInputChange}
            />
          </Col>
        </FormGroup>
        <FormGroup controlId="formControlsSelect">
          <Col componentClass={ControlLabel} sm={2}>
            <div className="info-title">
              Department
            </div>
          </Col>
          <Col sm={10}>
          <FormControl 
            componentClass="select" 
            name="departmentId"
            placeholder="select"
            value={props.currentEmployee.departmentId}
            onChange={props.handleInputChange}>
            {props.departments.map(department => (<option value={department.id} key={department.id}>{department.name}</option>))}
          </FormControl>
          </Col>
        </FormGroup>
        <Button bsStyle="success" block type="submit">Save</Button>
      </Form>
    </Panel>
  );
};

export default EditEmployee;
