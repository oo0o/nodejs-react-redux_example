import React, { Component } from 'react';

import { Link } from 'react-router';

// Import components
import LeftSideBar from './LeftSideBar';
import MainContent from './MainContent';

// Import bootstrap components
import Tab from 'react-bootstrap/lib/Tab';
import Row from 'react-bootstrap/lib/Row';

class Main extends Component {
  render() {
    return (
      <div>
        <Tab.Container id="left-tabs">
          <Row className="clearfix">
            <LeftSideBar />
            <MainContent children={this.props.children}/>
          </Row>
        </Tab.Container>
      </div>
    );
  }
}

export default Main;
