import React, { Component } from 'react';

// Import bootstrap components
import Col from 'react-bootstrap/lib/Col';

class MainContent extends Component {
  render() {
    return (
      <Col sm={9}>
          {this.props.children}
      </Col>
    );
  }
}

export default MainContent;
