import { bindActionCreators }         from 'redux';
import { connect }                    from 'react-redux';
import * as actionCreatorsDepartments from '../../actions/departments';
import DepartmentsGrid                from './DepartmentsGrid';

function mapStateToProps(state) {
  return {
    departments: state.departments
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({...actionCreatorsDepartments}, dispatch);
}

const _DepartmentsGrid = connect(mapStateToProps, mapDispatchToProps)(DepartmentsGrid);

export default _DepartmentsGrid;
