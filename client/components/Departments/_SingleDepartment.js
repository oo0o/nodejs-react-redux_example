import { bindActionCreators }         from 'redux';
import { connect }                    from 'react-redux';
import * as actionCreatorsDepartments from '../../actions/departments';
import SingleDepartment               from './SingleDepartment';

function mapStateToProps(state) {
  return {
         departments: state.departments,
    singleDepartment: state.singleDepartment
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators({...actionCreatorsDepartments}, dispatch);
}

const _SingleDepartment = connect(mapStateToProps, mapDispatchToProps)(SingleDepartment);

export default _SingleDepartment;
