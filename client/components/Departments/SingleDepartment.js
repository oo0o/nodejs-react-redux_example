import React, { Component } from 'react';
import Department from './Department';
import * as h from '../../lib/helpers';

import EditDepartment from './EditDepartment';
import CurrentDepartmentInfo from './CurrentDepartmentInfo';

class SingleDepartment extends Component {

  state = {
    currentDepartment: {
      name: ''
    }
  }
  
  componentWillMount() {
    this.props.fetchDepartments();
    this.props.fetchDepartment(+this.props.params.departmentId);
  }
  
  componentWillReceiveProps(nextProps) {
    this.setState({
      currentDepartment: {
        name: nextProps.singleDepartment.name
      }
    });
  }

  handleInputChange = (evt) => {
    const target = evt.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      currentDepartment: {...this.state.currentDepartment, [name]: value}
    });
  }

   handleUpdate = (evt) => {
    evt.preventDefault();
    const previousDepartment = h.findById(this.props.singleDepartment.id, this.props.departments);
    const updatedDepartment = h.updateItem(previousDepartment, this.state.currentDepartment);
    this.props.saveDepartment(updatedDepartment);
  }

  render() {
    return (
      <div className="single-department">
        <h2>Edit department</h2>
        <div className="info">
          <CurrentDepartmentInfo 
            singleDepartment={this.props.singleDepartment}
          />
          <EditDepartment 
            handleUpdate={this.handleUpdate}
            currentDepartment={this.state.currentDepartment}
            handleInputChange={this.handleInputChange}
          />
        </div>
      </div>
    );
  }
}

export default SingleDepartment;
