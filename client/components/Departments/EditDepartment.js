import React from 'react';

//import Bootstrap
import Panel from 'react-bootstrap/lib/Panel';
import Form from 'react-bootstrap/lib/Form';
import FormGroup from 'react-bootstrap/lib/FormGroup';
import Col from 'react-bootstrap/lib/Col';
import FormControl from 'react-bootstrap/lib/FormControl';
import ControlLabel from 'react-bootstrap/lib/ControlLabel';
import Button from 'react-bootstrap/lib/Button';

const EditDepartment = (props) => {
  return (
    <Panel header="New info" bsStyle="success">
      <Form horizontal onSubmit={props.handleUpdate}>
        <FormGroup>
          <Col componentClass={ControlLabel} sm={2}>
            <div className="info-title">
              Name
            </div>
          </Col>
          <Col sm={10}>
            <FormControl
              type="text"
              name="name"
              value={props.currentDepartment.name || ''}
              placeholder="Enter department name"
              onChange={props.handleInputChange}
            />
          </Col>
        </FormGroup>
        <Button bsStyle="success" block type="submit">Save</Button>
      </Form>
    </Panel>
  );
};

export default EditDepartment;
