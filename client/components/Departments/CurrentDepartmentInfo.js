import React from 'react';

//import bootstrap
import Panel from 'react-bootstrap/lib/Panel';
import Form from 'react-bootstrap/lib/Form';
import FormGroup from 'react-bootstrap/lib/FormGroup';
import Col from 'react-bootstrap/lib/Col';
import FormControl from 'react-bootstrap/lib/FormControl';
import ControlLabel from 'react-bootstrap/lib/ControlLabel';

const CurrentDepartmentInfo = (props) => {
  return (
    <div>
      <Panel header="Current info" bsStyle="info">
        <Form horizontal>
          <FormGroup>
            <Col componentClass={ControlLabel} sm={2}>
              <div className="info-title">
                Id
              </div>
            </Col>
            <Col sm={10}>
              <FormControl
                value={props.singleDepartment.id || ''}
                disabled
              />
            </Col>
          </FormGroup>
          <FormGroup>
            <Col componentClass={ControlLabel} sm={2}>
              <div className="info-title">
                Name
              </div>
            </Col>
            <Col sm={10}>
              <FormControl
                value={props.singleDepartment.name || ''}
                disabled
              />
            </Col>
          </FormGroup>
        </Form>
      </Panel>
    </div>
  );
};

export default CurrentDepartmentInfo;
