import React from 'react';

// import bootstrap
import Panel from 'react-bootstrap/lib/Panel';
import Accordion from 'react-bootstrap/lib/Accordion';
import Form from 'react-bootstrap/lib/Form';
import FormGroup from 'react-bootstrap/lib/FormGroup';
import Col from 'react-bootstrap/lib/Col';
import FormControl from 'react-bootstrap/lib/FormControl';
import ControlLabel from 'react-bootstrap/lib/ControlLabel';

const AddDepartment = (props) => {
  return (
    <div className="grid-item">
      <Accordion>
        <Panel header="Add department" eventKey="1">
          <Form horizontal onSubmit={props.handleCreate}>
            <FormGroup>
              <Col componentClass={ControlLabel} sm={2}>
                <div className="info-title">
                  Name
                </div>
              </Col>
              <Col sm={10}>
                <FormControl
                  type="text"
                  name="name"
                  value={props.currentDepartment.name || ''}
                  placeholder="Enter department name"
                  onChange={props.handleInputChange}
                />
              </Col>
            </FormGroup>
            <div className="control-buttons">
              <button className="add" type="submit">Add</button>
            </div>
          </Form>
        </Panel>
      </Accordion>
    </div>
  );
};

export default AddDepartment;
