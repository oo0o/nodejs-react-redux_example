import React, { Component } from 'react';
import { Link } from 'react-router';
import { partial } from '../../lib/utils';

class Department extends Component {
  
  render() {
    const { department, i } = this.props;
    const destroyDepartment = partial(this.props.destroyDepartment, department.id);
    return (
        <div className='grid-item'>
          <div className="item-caption">
            <Link to={`/department/${department.id}`} className='link'>
              {department.name}
            </Link>
          </div>
          <div className="control-buttons">
            <Link to={`/department/${department.id}`} className="button">
              Edit
            </Link>
            <button className="delete" onClick={destroyDepartment}>Delete</button>
          </div>
        </div>
    );
  }
}

export default Department;
