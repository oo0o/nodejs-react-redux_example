import React, { Component } from 'react';

// import components
import Department    from './Department';
import AddDepartment from './AddDepartment';

class DepartmentsGrid extends Component {
  state = {
    currentDepartment: {
      name: ''
    }
  }

  componentWillMount() {
    this.props.fetchDepartments();
  }

  handleInputChange = (evt) => {
    const target = evt.target;
    const  value = target.value;
    const   name = target.name;
    this.setState({
      currentDepartment: {...this.state.currentDepartment, [name]: value}
    });
  }

  handleCreate = (evt) => {
    evt.preventDefault();
    const newDepartment = {...this.state.currentDepartment};
    this.props.createDepartment(newDepartment).then(
      this.setState({
        currentDepartment: {
          name: ''
        }
      })
    );
  }
  
  render() {
    return (
      <div className="grid">
        <AddDepartment 
          handleCreate={this.handleCreate}
          currentDepartment={this.state.currentDepartment}
          handleInputChange={this.handleInputChange}
        />
        {this.props.departments.map((department, i) => 
          <Department 
            {...this.props} 
            key={department.id} 
            department={department} 
          />
        )}
      </div>
    );
  }
}

export default DepartmentsGrid;
