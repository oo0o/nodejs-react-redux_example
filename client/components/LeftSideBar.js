import React, { Component } from 'react';

// Import bootstrap components
import Col from 'react-bootstrap/lib/Col';
import Nav from 'react-bootstrap/lib/Nav';
import LinkContainer from 'react-router-bootstrap/lib/LinkContainer';
import NavItem from 'react-bootstrap/lib/NavItem';

class LeftSideBar extends Component {
  render() {
    return (
      <Col sm={3} className='left-sidebar'>
        <Nav bsStyle="pills" stacked>
          <LinkContainer to='/departments'>
            <NavItem>
              <h3>Departments</h3>
            </NavItem>
          </LinkContainer>
          <LinkContainer to='/employees'>
            <NavItem>
              <h3>Employees</h3>
            </NavItem>
          </LinkContainer>
        </Nav>
      </Col>
    );
  }
}

export default LeftSideBar;
