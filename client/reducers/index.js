import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import departments from './departments';
import singleDepartment from './singleDepartment';

const rootReducer = combineReducers({
  departments,
  singleDepartment,
  employees,
  singleEmployee,
  routing: routerReducer
});

export default rootReducer;

