import * as types from '../constants/employees';
import initialState from './initialState';
import * as h from '../lib/helpers';

export default function employeeReducer(state = initialState.employees, action) {
  switch(action.type) {
    case types.FETCH_EMPLOYEES_SUCCESS:
      return action.employees;
    case types.DESTROY_EMPLOYEE_SUCCESS:
      return h.removeItem(state, action.employeeId);
    case types.SAVE_EMPLOYEE_SUCCESS:
      return h.updateList(state, action.employee);
    case types.CREATE_EMPLOYEE_SUCCESS:
      return h.addItem(state, action.employee);
    default:
      return state;
  }
}
