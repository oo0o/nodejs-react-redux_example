import * as types from '../constants/departments';
import initialState from './initialState';
import * as h from '../lib/helpers';

export default function departmentsReducer(state = initialState.departments, action) {
  switch(action.type) {
    case types.FETCH_DEPARTMENTS_SUCCESS:
      return action.departments;
    case types.DESTROY_DEPARTMENT_SUCCESS:
      return h.removeItem(state, action.departmentId);
    case types.SAVE_DEPARTMENT_SUCCESS:
      return h.updateList(state, action.department);
    case types.CREATE_DEPARTMENT_SUCCESS:
      return h.addItem(state, action.department);
    default:
      return state;
  }
}
