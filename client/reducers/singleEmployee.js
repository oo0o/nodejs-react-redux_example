import * as types from '../constants/employees';
import initialState from './initialState';
import * as h from '../lib/helpers';

export default function singleEmployeeReducer(state = initialState.singleEmployee, action) {
  switch(action.type) {
    case types.FETCH_EMPLOYEE_SUCCESS:
      return action.employee;
    case types.SAVE_EMPLOYEE_SUCCESS:
      return action.employee;
    default:
      return state;
  }
}
