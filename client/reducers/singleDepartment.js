import * as types from '../constants/departments';
import initialState from './initialState';
import * as h from '../lib/helpers';

export default function singleDepartmentReducer(state = initialState.singleDepartment, action) {
  switch(action.type) {
    case types.FETCH_DEPARTMENT_SUCCESS:
      return action.department;
    case types.SAVE_DEPARTMENT_SUCCESS:
      return action.department;
    default:
      return state;
  }
}
