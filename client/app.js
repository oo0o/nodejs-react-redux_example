import React from 'react';

import { render } from 'react-dom';

// Import styles
import css from './styles/style.styl';

/**
 * Import components
 */
// Main component
import Main from './components/Main';

// Department components
import Departments       from './components/Departments/Departments';
import _DepartmentsGrid  from './components/Departments/_DepartmentsGrid';
import _SingleDepartment from './components/Departments/_SingleDepartment';

//Employee components
import Employees       from './components/Employees/Employees' ;
import _EmployeesGrid  from './components/Employees/_EmployeesGrid';
import _SingleEmployee from './components/Employees/_SingleEmployee';

// Import react-router deps
import { 
          Router, 
          Route, 
          IndexRoute, 
          browserHistory 
        }                 from 'react-router';
import { Provider }       from 'react-redux';
import store, { history } from './store/configureStore';

const router = (
  <Provider store={store}>
    <Router history={history}>
      <Route path="/" component={Main}>
         <Route path="/departments" component={Departments}>
          <IndexRoute component={_DepartmentsGrid} />
          <Route path="/department/:departmentId" component={_SingleDepartment} />
        </Route>
        <Route path="/employees" component={Employees}>
          <IndexRoute component={_EmployeesGrid} />
          <Route path="/employee/:employeeId" component={_SingleEmployee} />
        </Route>
      </Route>
    </Router>
  </Provider>
);

render(router, document.getElementById('app'));

