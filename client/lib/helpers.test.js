import { addItem, findById, updateItem, removeItem } from './helpers';

test('addItem should add the passed Item to the list', () => {

  const startItems = [
    {id:1, name: 'one', isComplete: false},
    {id:2, name: 'two', isComplete: false}
  ]

  const newItem = {id:3, name: 'three', isComplete: false}

  const expected = [
    {id:1, name: 'one', isComplete: false},
    {id:2, name: 'two', isComplete: false},
    {id:3, name: 'three', isComplete: false},
  ]

  const result = addItem(startItems, newItem);

  expect(result).toEqual(expected);
})

test('addItem should should not mutate the existing Item array', () => {

  const startItems = [
    {id:1, name: 'one', isComplete: false},
    {id:2, name: 'two', isComplete: false}
  ]

  const newItem = {id:3, name: 'three', isComplete: false}

  const expected = [
    {id:1, name: 'one', isComplete: false},
    {id:2, name: 'two', isComplete: false},
    {id:3, name: 'three', isComplete: false},
  ]

  const result = addItem(startItems, newItem);

  expect(result).not.toBe(startItems)
})

test('findById should return the expected item from an array', () => {
  const startItems = [
    {id:1, name: 'one', isComplete: false},
    {id:2, name: 'two', isComplete: false}
  ]

  const expected = {id:2, name: 'two', isComplete: false}

  const result = findById(2, startItems);

  expect(result).toEqual(expected);
})

test('updateItem should update an item by id', () => {
  const startItems = [
    {id:1, name: 'one', isComplete: false},
    {id:2, name: 'two', isComplete: false},
    {id:3, name: 'three', isComplete: false}
  ]

  const updatedItem = {id:2, name: 'two', isComplete: true}

  const expectedItems = [
    {id:1, name: 'one', isComplete: false},
    {id:2, name: 'two', isComplete: true},
    {id:3, name: 'three', isComplete: false},
  ]

  const result = updateItem(startItems, updatedItem);

  expect(result).toEqual(expectedItems);
})

test('updateItem should not mutate the original array', () => {
  const startItems = [
    {id:1, name: 'one', isComplete: false},
    {id:2, name: 'two', isComplete: false},
    {id:3, name: 'three', isComplete: false}
  ]

  const updatedItem = {id:2, name: 'two', isComplete: true}

  const result = updateItem(startItems, updateItem);

  expect(result).not.toBe(startItems);
})

test('removeItem should remove an item by id', () => {
  const startItems = [
    {id:1, name: 'one', isComplete: false},
    {id:2, name: 'two', isComplete: false},
    {id:3, name: 'three', isComplete: false}
  ]

  const targetId = 2;
  const expectedItemds = [
    {id:1, name: 'one', isComplete: false},
    {id:3, name: 'three', isComplete: false}
  ]

  const result = removeItem(startItems, targetId);

  expect(result).toEqual(expectedItemds);
})

test('removeItem should not mutate the original array', () => {
  const startItems = [
    {id:1, name: 'one', isComplete: false},
    {id:2, name: 'two', isComplete: false},
    {id:3, name: 'three', isComplete: false}
  ]

  const targetId = 2;
  
  const result = removeItem(startItems, targetId);

  expect(result).not.toBe(startItems);
})

