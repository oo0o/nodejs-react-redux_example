import * as _url from './baseUrls';
const baseUrl = _url.employees;
const headers = {
  'Accept': 'application/json',
  'Content-Type': 'application/json'
}

class EmployeesApi {

  static fetchEmployees = () => fetch(baseUrl).then(res => res.json());

  static createEmployee = (employee) => fetch(baseUrl, {
    method: 'POST',
    headers,
    body: JSON.stringify(employee)
  }).then(res => res.json());

  static getEmployee = (id) => fetch(`${baseUrl}/${id}`).then(res => res.json());

  static saveEmployee = (employee) => fetch(`${baseUrl}/${employee.id}`, {
    method: 'PUT',
    headers,
    body: JSON.stringify(employee)
  }).then(res => res.json());

  static destroyEmployee = (employeeId) => fetch(`${baseUrl}/${employeeId}`, {
    method: 'DELETE',
    headers
  }).then(res => res.json());

}

export default EmployeesApi;
