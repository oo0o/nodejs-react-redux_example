import * as _url from './baseUrls';
const baseUrl = _url.departments;
const headers = {
  'Accept': 'application/json',
  'Content-Type': 'application/json'
}

class DepartmentsApi {

  static fetchDepartments = () => fetch(baseUrl).then(res => res.json());

  static createDepartment = (department) => fetch(baseUrl, {
    method: 'POST',
    headers,
    body: JSON.stringify(department)
  }).then(res => res.json());

  static getDepartment = (id) => fetch(`${baseUrl}/${id}`).then(res => res.json());

  static saveDepartment = (department) => fetch(`${baseUrl}/${department.id}`, {
    method: 'PUT',
    headers,
    body: JSON.stringify(department)
  }).then(res => res.json());

  static destroyDepartment = (departmentId) => fetch(`${baseUrl}/${departmentId}`, {
    method: 'DELETE',
    headers
  }).then(res => res.json());

}

export default DepartmentsApi;
