// EMPLOYEE list
export const FETCH_EMPLOYEES         = 'FETCH_EMPLOYEES';
export const FETCH_EMPLOYEES_SUCCESS = 'FETCH_EMPLOYEES_SUCCESS';
export const FETCH_EMPLOYEES_FAILURE = 'FETCH_EMPLOYEES_FAILURE';
export const RESET_EMPLOYEES         = 'RESET_EMPLOYEES';

// Create new EMPLOYEE
export const CREATE_EMPLOYEE         = 'CREATE_EMPLOYEE';
export const CREATE_EMPLOYEE_SUCCESS = 'CREATE_EMPLOYEE_SUCCESS';
export const CREATE_EMPLOYEE_FAILURE = 'CREATE_EMPLOYEE_FAILURE';
export const RESET_NEW_EMPLOYEE      = 'RESET_NEW_EMPLOYEE';

// Update employee
export const SAVE_EMPLOYEE         = 'SAVE_EMPLOYEE';
export const SAVE_EMPLOYEE_SUCCESS = 'SAVE_EMPLOYEE_SUCCESS';
export const SAVE_EMPLOYEE_FAILURE = 'SAVE_EMPLOYEE_FAILURE';

// Fetch EMPLOYEE
export const FETCH_EMPLOYEE         = 'FETCH_EMPLOYEE';
export const FETCH_EMPLOYEE_SUCCESS = 'FETCH_EMPLOYEE_SUCCESS';
export const FETCH_EMPLOYEE_FAILURE = 'FETCH_EMPLOYEE_FAILURE';
export const RESET_ACTIVE_EMPLOYEE  = 'RESET_ACTIVE_EMPLOYEE';

// DESTROY EMPLOYEE
export const DESTROY_EMPLOYEE         = 'DESTROY_EMPLOYEE';
export const DESTROY_EMPLOYEE_SUCCESS = 'DESTROY_EMPLOYEE_SUCCESS';
export const DESTROY_EMPLOYEE_FAILURE = 'DESTROY_EMPLOYEE_FAILURE';
export const RESET_DESTROYED_EMPLOYEE = 'RESET_DESTROYED_EMPLOYEE';
