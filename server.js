var express = require('express'),
    bodyParser = require('body-parser'),
    http = require('http'),
    path = require('path'),
    Sequelize = require('sequelize'),
    _ = require('lodash');

sequelize = new Sequelize('sqlite://' + path.join(__dirname, 'aTeam.sqlite'), {
  dialect: 'sqlite',
  storage: path.join(__dirname, 'aTeam.sqlite')
});

// Department schema
Department = sequelize.define('department', {
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  name: {
    type: Sequelize.STRING
  }
});

// Employee schema
Employee = sequelize.define('employee', {
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  firstName: {
    type: Sequelize.STRING
  },
  lastName: {
    type: Sequelize.STRING
  },
  departmentId: {
    type: Sequelize.STRING
  }
});

sequelize.sync()
  .then(function() {
    Department.truncate();
  })
  .then(function() {
    Employee.truncate();
  })
  .then(function() {

  Department.create({
    name: "Parkside Police"
  });

  Department.create({
    name: "Norfolk Highway"
  });

  Employee.create({
    firstName: "Bob",
    lastName: "Smith",
    departmentId: "1"
  });

  Employee.create({
    firstName: "John",
    lastName: "Draper",
    departmentId: "2"
  });

  Employee.create({
    firstName: "Mark",
    lastName: "Benson",
    departmentId: "1"
  });

}).catch(function(e) {
  console.log("ERROR SYNCING WITH DB", e);
});

var app = module.exports = express();
app.set('port', process.env.PORT || 8000);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

// DEPARTMENTS API
app.route('/api/departments')
  .get(function(req, res) {
    Department.findAll().then(function(departments) {
      res.json(departments);
    });
  })
  .post(function(req, res) {
    var department = Department.build(_.pick(req.body, ['name']));
    department.save().then(function(department){
      res.json(department);
    });
  });

app.route('/api/departments/:department_id')
  .get(function(req, res) {
    Department.findById(req.params.department_id).then(function(department) {
      res.json(department);
    });
  })
  .put(function(req, res) {
    Department.findById(req.params.department_id).then(function(department) {
      department.update(_.pick(req.body, ['name'])).then(function(department) {
        res.json(department);
      });
    });
  })
  .delete(function(req, res) {
    Department.findById(req.params.department_id).then(function(department) {
      department.destroy().then(function(department) {
        res.json(department);
      });
    });
  });

// EMPLOYEES API
app.route('/api/employees')
  .get(function(req, res) {
    Employee.findAll().then(function(employees) {
      res.json(employees);
    })
  })
  .post(function(req, res) {
    var employee = Employee.build(_.pick(req.body, ['firstName', 'lastName', 'departmentId']));
    employee.save().then(function(employee){
      res.json(employee);
    });
  });

app.route('/api/employees/:employee_id')
  .get(function(req, res) {
    Employee.findById(req.params.employee_id).then(function(employee) {
      res.json(employee);
    });
  }) 
  .put(function(req, res) {
    Employee.findById(req.params.employee_id).then(function(employee) {
      employee.update(_.pick(req.body, ['firstName', 'lastName', 'departmentId'])).then(function(employee) {
        res.json(employee);
      });
    });
  })
  .delete(function(req, res) {
    Employee.findById(req.params.employee_id).then(function(employee) {
      employee.destroy().then(function(employee) {
        res.json(employee);
      });
    });
  });

const webpack = require('webpack');
const webpackMiddleware = require('webpack-dev-middleware');
const webpackHotMiddleware = require('webpack-hot-middleware');
const config = require('./webpack.config.js');

const isDeveloping = process.env.NODE_ENV !== 'production';
const port = isDeveloping ? 3000 : process.env.PORT;

if (isDeveloping) {
  const compiler = webpack(config);
  const middleware = webpackMiddleware(compiler, {
    publicPath: config.output.publicPath,
    contentBase: 'client',
    stats: {
      colors: true,
      hash: false,
      timings: true,
      chunks: false,
      chunkModules: false,
      modules: false
    }
  });

  app.use(middleware);
  app.use(webpackHotMiddleware(compiler));
  app.get('*', function response(req, res) {
    res.write(middleware.fileSystem.readFileSync(path.join(__dirname, 'public/index.html')));
    res.end();
  });
} else {
  app.use(express.static(__dirname + '/public'));
  app.get('*', function response(req, res) {
    res.sendFile(path.join(__dirname, 'public/index.html'));
  });
}

// Starting express server
http.createServer(app).listen(app.get('port'), function () {
  console.log('Express server listening on port ' + app.get('port'));
});
